include_path := includes
includes := $(wildcard $(include_path)/*.html.tera)

pages := public/index.html public/cv.html
recipes := public/recipes/index.html public/recipes/bread.html public/recipes/focaccia.html public/recipes/pizza-dough.html
context := context.toml

default: $(pages) $(recipes)

$(pages): public/%.html: %.html.tera $(includes)
	tera --include-path $(include_path) --template ${subst public/,,$@}.tera $(context) > $@

$(recipes): public/recipes/%.html: $(includes) recipes/%.md recipes/recipe.html
	pandoc ${subst html,md,${subst public/,,$@}} --template recipes/recipe.html > $@

recipes/recipe.html: recipes/recipe.html.tera $(includes)
	tera --include-path $(include_path) --template recipes/recipe.html.tera $(context) > $@

clean:
	rm -rf $(pages)
	rm -rf $(recipes)
	rm -rf recipes/recipe.html

watch:
	while true; do \
		$(MAKE) $(WATCHMAKE); \
		inotifywait -qre close_write .; \
	done

server:
	python3 -m http.server -d public

deploy: default
	rsync -ravP public/ tardis:www
