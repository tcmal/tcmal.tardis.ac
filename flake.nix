{
  description = "my site :3";
  inputs = {
    tera-cli = {
      url = "github:chevdor/tera-cli";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    nixpkgs,
    tera-cli,
    ...
  }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {inherit system;};
  in {
    devShells.${system}.default = pkgs.mkShell {
      buildInputs = with pkgs; [gnumake inotify-tools pandoc (tera-cli.packages.${system}.teracli)];
    };
  };
}
