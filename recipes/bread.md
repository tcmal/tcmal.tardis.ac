% Bread

Ingredients:

  * 340g bread flour
  * 225ml water
  * 20g honey
  * 15g oil
  * 7g yeast
  * 7g salt

Method:

1. Measure the water, oil, honey, and yeast into a bowl and let rest for 5mins.
2. Mix the flour and salt in a seperate bowl, then add to the water/yeast mixture.
3. Mix and squeeze through fingers to combine.
4. Leave to rest for 5min.
5. Do a strengh building fold, then rest for another 5 minutes. Repeat until folded 3 times total.
6. Proof for approximately 1hr.
7. Flip dough onto surface and degas thoroughly.
8. Pre-shape into boule, and leave to bench rest for 5-10 minutes.
9. Shape into batard, and place in loaf tin.
10. Proof for 1.5-2hrs.
11. Bake at 190C for approx 30mins.
