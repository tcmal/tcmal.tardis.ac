% Focaccia

Makes a tray or pan's worth

Adapted from [Brian Lagerstrom's](https://youtu.be/OC2mbadj8gQ) shorter recipe.

Ingredients:

  * 360g water
  * 4g yeast
  * 25g oil
  * 10g sugar
  * 450g bread flour
  * 12g salt

Method:

  1. Combine all ingredients in bowl.
  2. Mix with hand for 2-3 minutes, squeezing to make sure its consistent.
  3. Rest for 15min.
  4. Do a strengh building fold.
  5. Oil pan, place dough in and dimple.
  6. Prove for 45min.
  7. Dimple again.
  8. Prove for 45min.
  9. (Optional) Sprinkle flaky salt on top.
  10. Bake at 260C for 15, optionally sprinkling garlic or herbs after around 10min to avoid them burning.
