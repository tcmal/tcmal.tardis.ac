% Pizza Dough

Makes 1 12" pizza.

Ingredients:

  * 130ml warm water
  * 3.5g yeast
  * 12g sugar
  * 10g oil
  * 3g salt
  * 180g flour

Method:

  1. Combine water, oil, yeast & sugar.
  2. Rest for 5min.
  3. Add the rest. Stir then knead for 5 mins.
  4. Place in oiled container and cover for 1hr / until doubled.
  5. Knock back & shape. Rest for a few minutes.
  6. Brush top with olive oil.
  7. Top and bake at 250C for 7-9 mins.

## Pizza sauce

From [Brian Lagerstrom's NY pizza video](https://www.youtube.com/watch?v=t4t3bqb_91E)

Ingredients:

  * Passata
  * 1/2tsp sugar
  * 1/2tsp salt
  * pinch of chilli flakes
  * 2 cloves of garlic
  * 2tbsp butter

  1. Smash garlic and simmer on medium heat with butter until starting to brown.
  2. Add passata, sugar and salt.
  3. Reduce heat slightly and reduce by half, and add chilli flakes.
  4. Remove garlic cloves and spread on pizza.
